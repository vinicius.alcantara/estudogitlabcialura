FROM python:3.6
COPY . /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN python3.6 -m pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
EXPOSE 8000
#USER app
CMD ["gunicorn", "to_do.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "3"]